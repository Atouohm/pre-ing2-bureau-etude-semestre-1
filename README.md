PRE-ING2 Bureau d'étude Avril 2023

Membres :
- Julien GITTON (G1)
- Matheo BIOUDI (G1)
- Arthur ACCINI (G1)
- Dorian BARRERE (G1)

Les dépendances :
- Commandes/Outils Linux :
  - gnuplot (version 5.4 ou ultérieure)
- Librairies C :
  - stdio
  - stdlib
  - string
  - math
  - time
  - gsl/gsl_math
  - gsl/gsl_const_mksa
  - gsl/gsl_multiroots

Informations :
1) L'application doit être lancée grâce au fichier ./executable.
2) Lancer l'application doit se faire uniquement lorsque le dossier courant est celui où se trouve le fichier ./executable.
3) --aide pour afficher l'aide
4) Les fichiers générés sont trouvable dans le dossier ./fichiersGeneres.

Les options génériques :
- --aide : affiche l'aide détaillée
- -e <mode{1|2|3}> : chercher un point d'équilibre
  - 1 : rechercher dans le cadre de l'approximation dipolaire
  - 2 : rechercher dans le cadre exact
  - 3 : lance tous les modes précédents

Les options de génération :
- -p <mode{1|2|3|4}> : utiliser la génération dans l'approximation dipolaire
  - 1 : génère les fichiers du Champ Électrique
  - 2 : génère les fichiers du Potentiel
  - 3 : génère le graphique des LDC (Lignes De Champ) et des EQ (Équipotentielles)
  - 4 : lance tous les modes précédents
- -c <mode{1|2|3}> : utiliser la génération cartésienne
  - 1 : génère les fichiers du Champ Électrique
  - 2 : génère les fichiers du Potentiel
  - 3 : lance tous les modes précédents

Les options de paramétrage :
- OGLIGATOIRES POUR LE MODE -p ET LE MODE -e {1|3} :
  1) -d <valeur (Mètre)> : la distance entre les 2 charges
  2) -q <valeur (Coulomb)> : la valeur absolue des 2 charges
- OGLIGATOIRE POUR LE MODE -c ET LE MODE -e {2|3} :
  1) -l <valeur charge n°1 (Coulomb)> <coordonnée X charge n°1 (Mètre)> <coordonnée Y charge n°1 (Mètre)> ... : la liste des charges du système (le nombre de charges n'est pas limité)