/* on inclut tout ce dont à a besoin */
#include "librairiesExternes.h"

double resterDansPI(double angle){
	angle = angle - (int)angle/(int)(2*M_PI)*(2*M_PI); // on ramène l'angle modulo 2PI

	/* et on le ramène à l'intervalle ]-PI;PI] */
	if(angle <= -M_PI){
		return angle = 2*M_PI + angle;
	} else if(angle > M_PI){
		return -(2*M_PI - angle);
	} else{
		return angle;
	}
}

double angleCooPolaires(double x, double y){
	if(!(x == 0 && y <= 0)){ // on est pas en (0,0)
		return resterDansPI(2*atan(y/(x + sqrt(pow(x, 2) + pow(y, 2)))));
	} else if(x == 0 && y == 0){ // sinon si on est en (0,0)
		return 0;
	} else{ // sinon dans tous les autres cas
		return -M_PI;
	}
}

void afficher_statusSJ(gsl_multiroot_fsolver* solveur){
	printf ("   Un point d'équilibre électrostatique a été trouvé : (%e ; %e) -> Ex = %e et Ey = %e\n", gsl_vector_get(solveur->x, 0), gsl_vector_get(solveur->x, 1), gsl_vector_get(solveur->f, 0), gsl_vector_get(solveur->f, 1));
}

void afficher_statusAJ(gsl_multiroot_fdfsolver* solveur){
	printf ("   Un point d'équilibre électrostatique a été trouvé : (%e ; %e) -> Ex = %e et Ey = %e\n", gsl_vector_get(solveur->x, 0), gsl_vector_get(solveur->x, 1), gsl_vector_get(solveur->f, 0), gsl_vector_get(solveur->f, 1));
}