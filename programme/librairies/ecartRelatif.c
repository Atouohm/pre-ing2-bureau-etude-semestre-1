/* on inclut tout ce dont à a besoin */
#include "librairiesExternes.h"
#include "ecartRelatif.h"
#include "fonctionsPolaires.h"
#include "fonctionsCartesiennes.h"
#include "utilitaires.h"

double distanceVersionCooCartesiennesPourEcartRelatif(COOCARTESIENNES point1, COOCARTESIENNES point2){
	return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
}

// retourne 1 si selon le rayon minimal le point donné est trop proche de une charge dans la liste donnée, auteur : Dorian BARRERE
int estTropProcheDeUneChargePourEcartRelatif(COOCARTESIENNES point, LISTECHARGESENCARTESIEN charges, double rayonMinimal){
	int resultat = 0;

	for(int i=0 ; i<charges.nombre ; i++){ // on parcourt toutes les charges
		if(distanceVersionCooCartesiennesPourEcartRelatif(point, charges.tableauDesCharges[i].coo) < rayonMinimal){
			resultat = 1;
		}
	}

	return resultat;
}

int genererGraphiqueEcartRelatif(){
	double d = GSL_CONST_MKSA_ANGSTROM;
	double q = GSL_CONST_MKSA_ELECTRON_CHARGE;

	float x_max = d/2 * 2.2;
	float x_min = -x_max;
	float y_max = x_max;
	float y_min = x_min;

	float recurence = 50;

	FILE* fichier = fopen("./fichiersGeneres/ecartRelatif.dat", "w"); // on ouvre/crée le fichier de données pour le potentiel
	if(fichier==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf(ROUGE "\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n" NORMAL);
		return 0;
	}

	LISTECHARGESENCARTESIEN chargesCartesiennes;
	chargesCartesiennes.nombre = 2;
	chargesCartesiennes.tableauDesCharges = (CHARGEENCARTESIEN*)malloc(chargesCartesiennes.nombre*sizeof(CHARGEENCARTESIEN));
	chargesCartesiennes.tableauDesCharges[0].coo.x = d/2;
	chargesCartesiennes.tableauDesCharges[0].coo.y = 0;
	chargesCartesiennes.tableauDesCharges[0].valeur = q;
	chargesCartesiennes.tableauDesCharges[1].coo.x = -d/2;
	chargesCartesiennes.tableauDesCharges[1].coo.y = 0;
	chargesCartesiennes.tableauDesCharges[1].valeur = -q;

	LISTECHARGESENPOLAIRE chargesPolaires;
	chargesPolaires.nombre = 2;
	chargesPolaires.tableauDesCharges = (CHARGEENPOLAIRE*)malloc(chargesPolaires.nombre*sizeof(CHARGEENPOLAIRE));
	chargesPolaires.tableauDesCharges[0].coo.r = d/2;
	chargesPolaires.tableauDesCharges[0].coo.theta = 0;
	chargesPolaires.tableauDesCharges[0].valeur = q;
	chargesPolaires.tableauDesCharges[1].coo.r = d/2;
	chargesPolaires.tableauDesCharges[1].coo.theta = M_PI;
	chargesPolaires.tableauDesCharges[1].valeur = -q;

	fprintf(fichier, "# x (m) ; y (m) ; r (m) ; θ (rad : ]-PI;PI]) ; ecartRelatif (V)\n"); // l'en tête

	COOCARTESIENNES pointCartesien;
	COOPOLAIRES pointPolaire;

	/* on parcourt tout les points du maillage */
	for(float i=x_min ; i<=x_max ; i=i+(x_max+fabsf(x_min))/recurence){
		for(float j=y_min ; j<=y_max ; j=j+(y_max+fabsf(y_min))/recurence){

			pointCartesien.x = i;
			pointCartesien.y = j;

			pointPolaire.r = sqrt(pow(i, 2) + pow(j, 2));
			pointPolaire.theta = angleCooPolaires(i, j);

			if(!estTropProcheDeUneChargePourEcartRelatif(pointCartesien, chargesCartesiennes, x_max/25) && pointPolaire.r>x_max/5){
				fprintf(fichier, "%e;%e;%e;%e;%e\n", i, j, pointPolaire.r, pointPolaire.theta, fabsf(potentielVersionCooPolaires(pointPolaire, chargesPolaires) - potentielVersionCartesienne(pointCartesien, chargesCartesiennes))/fabs(potentielVersionCartesienne(pointCartesien, chargesCartesiennes)));
			}
		}
	}

	fclose(fichier); // on ferme le fichier

	char* demande = malloc(99*sizeof(char)); // on génère la demande
	sprintf(demande, "%s %e %e", "./programme/scripts/ecartRelatif.sh", d, x_max); // le coef 0.005 permet d'avoir un pas raisonnable, le baisser augementera le nombre d'équipotentielles
	system(demande); // on envoie la demande

	free(chargesCartesiennes.tableauDesCharges);
	free(chargesPolaires.tableauDesCharges);
	free(demande);

	return 1;
}
