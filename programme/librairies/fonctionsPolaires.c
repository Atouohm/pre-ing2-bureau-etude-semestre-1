/* on inclut tout ce dont à a besoin */
#include "librairiesExternes.h"
#include "fonctionsPolaires.h"
#include "utilitaires.h"

/* fonctions pour le calcul de la distance, du potentiel et du champ */

double distanceVersionCooPolaires(COOPOLAIRES point1, COOPOLAIRES point2){
	return sqrt(pow(point1.r*cos(point1.theta) - point2.r*cos(point2.theta), 2) + pow(point1.r*sin(point1.theta) - point2.r*sin(point2.theta), 2));
}

double potentielVersionCooPolaires(COOPOLAIRES point, LISTECHARGESENPOLAIRE charges){
	double d = distanceVersionCooPolaires(charges.tableauDesCharges[0].coo, charges.tableauDesCharges[1].coo);
	double q = fabs(charges.tableauDesCharges[0].valeur); // on suppose que les 2 ont la même charge opposée
	return (COULOMB*q*d*cos(point.theta))/pow(point.r, 2); // formule valide dans le cadre de l'approximation dipolaire
}

VECTEUR2DPOLAIRE champVersionCooPolaires(COOPOLAIRES point, LISTECHARGESENPOLAIRE charges){
	double d = distanceVersionCooPolaires(charges.tableauDesCharges[0].coo, charges.tableauDesCharges[1].coo);
	double q = fabs(charges.tableauDesCharges[0].valeur);
	double Er = (2*COULOMB*q*d*cos(point.theta))/pow(point.r, 3); // composante en r
	double Etheta = (COULOMB*q*d*sin(point.theta))/pow(point.r, 3); // composante en theta

	VECTEUR2DPOLAIRE vecteur;
	vecteur.longueur = sqrt(pow(Er, 2) + pow(Etheta, 2));
	vecteur.angle = resterDansPI(angleCooPolaires(Er, Etheta) + point.theta);
	return vecteur;
}

/* fonctions de génération des fichiers */

int genererFichiersPotentielDansApproximationDipolaire(double d, double q){
	/* on défninit le cadre dynamiquement en fonction de l'écartement des charges */
	float x_max = d/2 * 2.2; // valeur de base (hors tests) : d*20, pour avoir la même échelle que cas exact : d/2 * 2.2
	float x_min = -x_max;
	float y_max = x_max;
	float y_min = x_min;

	double potentielMax = 0; // pour l'instant 0, il nous servira pour construire dynamiquement des EQ lisibles

	float recurence = 50; // pour le mesh, on ne va pas calculer V partout

	COOPOLAIRES point;

	FILE* fichier = fopen("./fichiersGeneres/approximationDipolaire_potentiel.dat", "w"); // on ouvre/crée le fichier de données pour le potentiel
	if(fichier==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf(ROUGE "\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n" NORMAL);
		return 0;
	}

	/* ici la liste des charges est imposée, seule la distance les séparant et leurs chages peuvent être modifées par l'utilisateur */
	LISTECHARGESENPOLAIRE charges;
	charges.nombre = 2;
	charges.tableauDesCharges = (CHARGEENPOLAIRE*)malloc(charges.nombre*sizeof(CHARGEENPOLAIRE));
	charges.tableauDesCharges[0].coo.r = d/2;
	charges.tableauDesCharges[0].coo.theta = 0;
	charges.tableauDesCharges[0].valeur = q;
	charges.tableauDesCharges[1].coo.r = d/2;
	charges.tableauDesCharges[1].coo.theta = M_PI;
	charges.tableauDesCharges[1].valeur = -q;

	fprintf(fichier, "# x (m) ; y (m) ; r (m) ; θ (rad : ]-PI;PI]) ; potentiel (V)\n"); // l'en tête

	/* on parcourt tout les points du maillage */
	for(float i=x_min ; i<=x_max ; i=i+(x_max+fabsf(x_min))/recurence){
		for(float j=y_min ; j<=y_max ; j=j+(y_max+fabsf(y_min))/recurence){
			point.r = sqrt(pow(i, 2) + pow(j, 2));
			point.theta = angleCooPolaires(i, j);

			if(distanceVersionCooPolaires(point, charges.tableauDesCharges[0].coo) > x_max/25 && distanceVersionCooPolaires(point, charges.tableauDesCharges[1].coo) > x_max/25 && point.r > x_max/2.25){ // si on est "trop près" d'une charge on ne calcule pas le potentiel (dont les valeurs absolues tendent vers l'infini) // valeurs de base (hors tests) : d(/1) et pas de && point.r > x_max/2.25
				if(potentielMax < fabs(potentielVersionCooPolaires(point, charges))){
					potentielMax = fabs(potentielVersionCooPolaires(point, charges));
				}
				fprintf(fichier, "%e;%e;%e;%e;%e\n", i, j, point.r, point.theta, potentielVersionCooPolaires(point, charges));
			}
		}
	}

	fclose(fichier); // on ferme le fichier

	char* demande = malloc(99*sizeof(char)); // on génère la demande
	sprintf(demande, "%s %e %e %e", "./programme/scripts/approximationDipolaire_potentiel.sh", d, potentielMax*0.1, potentielMax*0.005); // le coef 0.005 permet d'avoir un pas raisonnable, le baisser augementera le nombre d'équipotentielles
	system(demande); // on envoie la demande

	free(charges.tableauDesCharges);
	free(demande);
	return 1;
}

// parfaitement similaire à la fonction précédente mais pour le champ électrique
int genererFichiersChampDansApproximationDipolaire(double d, double q){
	float x_max = d*20; // valeur de base (hors tests) : d*20, pour avoir la même échelle que cas exact : d/2 * 2.2
	float x_min = -x_max;
	float y_max = x_max;
	float y_min = x_min;
	double longueurMax; // ici on prend la longueur du vecteur le plus grand pour ensuite obtenir un coef de mise à l'échelle pour avoir des vecteurs d'une taille raisonnable

	float recurence = 20;

	COOPOLAIRES point;

	FILE* fichier = fopen("./fichiersGeneres/approximationDipolaire_champ.dat", "w");
	if(fichier==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf(ROUGE "\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n" NORMAL);
		return 0;
	}

	LISTECHARGESENPOLAIRE charges;
	charges.nombre = 2;
	charges.tableauDesCharges = (CHARGEENPOLAIRE*)malloc(charges.nombre*sizeof(CHARGEENPOLAIRE));
	charges.tableauDesCharges[0].coo.r = d/2;
	charges.tableauDesCharges[0].coo.theta = 0;
	charges.tableauDesCharges[0].valeur = q;
	charges.tableauDesCharges[1].coo.r = d/2;
	charges.tableauDesCharges[1].coo.theta = M_PI;
	charges.tableauDesCharges[1].valeur = -q;

	fprintf(fichier, "# x (m) ; y (m) ; r (m) ; θ (degré : ]-180;180]) ; norme du vecteur (N/C) ; angle du vecteur par rapport à Ox (rad : ]-PI;PI])\n");

	for(float i=x_min ; i<=x_max ; i=i+(x_max+fabsf(x_min))/recurence){
		for(float j=y_min ; j<=y_max ; j=j+(y_max+fabsf(y_min))/recurence){
			point.r = sqrt(pow(i, 2) + pow(j, 2));
			point.theta = angleCooPolaires(i, j);

			if(distanceVersionCooPolaires(point, charges.tableauDesCharges[0].coo) > 5*d && distanceVersionCooPolaires(point, charges.tableauDesCharges[1].coo) > 5*d){ // valeurs de base (hors tests) : 5*d et pas de && point.r > x_max/2.25
				if(longueurMax<champVersionCooPolaires(point, charges).longueur){
					longueurMax = champVersionCooPolaires(point, charges).longueur;
				}
				fprintf(fichier, "%e;%e;%e;%e;%e;%e\n", i, j, point.r, point.theta, champVersionCooPolaires(point, charges).longueur, champVersionCooPolaires(point, charges).angle*180/M_PI);
			}
		}
	}

	fclose(fichier); // on ferme le fichier

	char* demande = malloc(99*sizeof(char));
	sprintf(demande, "%s %e %e", "./programme/scripts/approximationDipolaire_champ.sh", d, longueurMax/(2.9*d)); // le coef longueurMax/(2.9*d) de "mise à une échelle lisible" permet d'avoir le plus grand vecteur comme ayant la longeur 2.9*d // valeur de base (hors tests) : longueurMax/(2.9*d), pour les tests de mise à la même échelle que la version cartésienne : longueurMax/(0.1*x_max)
	system(demande);

	free(charges.tableauDesCharges);
	free(demande);
	return 1;
}

int genererGrapheLdcEtEq(double d){
	char* demande = malloc(99*sizeof(char));
	sprintf(demande, "%s %e", "./programme/scripts/approximationDipolaire_LdcEtEq.sh", d);
	system(demande); // on envoie la demande

	free(demande);

	return 1;
}

/* fonctions nécessaires aux fonctions qui permettent de trouver un point d'équilibre. Elles sont toutes fortement inspirées de la documentation de la librairie GSL section "Multidimensional Root-Finding" */

struct parametres{
	double q;
	double d;
};

// fonction qui représente la fonction du champ électrique, auteur : Dorian BARRERE
int champPolaire_f(const gsl_vector* x, void* parametres, gsl_vector* f){
	double d = ((struct parametres*) parametres)->d;
	double q = ((struct parametres*) parametres)->q;

	/* on récupère le point où on veut calculer */
	const double x0 = gsl_vector_get(x, 0);
	const double x1 = gsl_vector_get(x, 1);

	/* on calcule */
	const double y0 = (2*COULOMB*q*d*cos(x1))/pow(x0, 3);
	const double y1 = (COULOMB*q*d*sin(x1))/pow(x0, 3);

	/* et on met à jour le vecteur f(x) */
	gsl_vector_set(f, 0, y0);
	gsl_vector_set(f, 1, y1);

	return GSL_SUCCESS;
}

// fonction représentant la jacobienne de la fonction que représente la fonction précédente, auteur : Dorian BARRERE
int champPolaire_df(const gsl_vector* x, void* parametres, gsl_matrix* J){
	double d = ((struct parametres*) parametres)->d;
	double q = ((struct parametres*) parametres)->q;

	const double x0 = gsl_vector_get(x, 0);
	const double x1 = gsl_vector_get(x, 1);


	const double df00 = (-6*COULOMB*q*d*cos(x1))/pow(x0, 4);
	const double df01 = (-2*COULOMB*q*d*sin(x1))/pow(x0, 3);
	const double df10 = (-3*COULOMB*q*d*sin(x1))/pow(x0, 4);
	const double df11 = (COULOMB*q*d*cos(x1))/pow(x0, 3);

	gsl_matrix_set (J, 0, 0, df00);
	gsl_matrix_set (J, 0, 1, df01);
	gsl_matrix_set (J, 1, 0, df10);
	gsl_matrix_set (J, 1, 1, df11);

	return GSL_SUCCESS;
}

// fonction qui représente la jacobienne de la fonction du champ électrique AINSI QUE la fonction elle même, auteur : Dorian BARRERE
int champPolaire_fdf(const gsl_vector* x, void* parametres, gsl_vector* f, gsl_matrix* J){

	champPolaire_f(x, parametres, f);
	champPolaire_df(x, parametres, J);

	return GSL_SUCCESS;
}

/* fonctions qui permettent de trouver un point d'équilibre. Elles sont toutes fortement inspirées de la documentation de la librairie GSL section "Multidimensional Root-Finding" */

int pointEquilibreDansApproximationDipolaireSJ(double d, double q){
	const gsl_multiroot_fsolver_type* typeSolveur;
	gsl_multiroot_fsolver* solveur;

	int status;
	size_t iteration = 0;

	const size_t dimension = 2;
	struct parametres parametres = {d, q};
	gsl_multiroot_function f = {&champPolaire_f, dimension, &parametres};

	double x_initial[2] = {d, M_PI/2};
	gsl_vector* x = gsl_vector_alloc(dimension);

	/* on prend 1 point initial */
	gsl_vector_set(x, 0, x_initial[0]);
	gsl_vector_set(x, 1, x_initial[1]);

	typeSolveur = gsl_multiroot_fsolver_hybrids;
	solveur = gsl_multiroot_fsolver_alloc(typeSolveur, 2);
	gsl_multiroot_fsolver_set(solveur, &f, x);

	do{ // on itère la méthode le tant qu'il n'y a pas d'erreur, qu'on ne dépasse pas 100 itérations, et qu'on ne sort pas du cadre
		iteration++;
		status = gsl_multiroot_fsolver_iterate(solveur);

		if(status){break;}

		status = gsl_multiroot_test_residual(solveur->f, 1e-64);
	} while(status == GSL_CONTINUE && iteration < 100 && fabs(gsl_vector_get(solveur->x, 0)*cos(gsl_vector_get(solveur->x, 1))) < 20*d && fabs(gsl_vector_get(solveur->x, 0)*sin(gsl_vector_get(solveur->x, 1))) < 20*d);

	if(status == 0 && fabs(gsl_vector_get(solveur->x, 0)*cos(gsl_vector_get(solveur->x, 1))) < 20*d && fabs(gsl_vector_get(solveur->x, 0)*sin(gsl_vector_get(solveur->x, 1))) < 20*d){
		afficher_statusSJ(solveur);
	} else{
		printf("Aucun équilibre électrostatique n'a été trouvé.\n");
	}

	/* on oublie pas de libérer */
	gsl_multiroot_fsolver_free(solveur);
	gsl_vector_free(x);

	return 1;
}

// tout à fait similaire dans la forme à la fonction précédente sauf qu'on fournit ici la jacobienne
int pointEquilibreDansApproximationDipolaireAJ(double d, double q){
	const gsl_multiroot_fdfsolver_type* typeSolveur;
	gsl_multiroot_fdfsolver* solveur;

	int status;
	size_t iteration = 0;

	const size_t dimension = 2;
	struct parametres parametres = {d, q};
	gsl_multiroot_function_fdf f = {&champPolaire_f, &champPolaire_df, &champPolaire_fdf, dimension, &parametres};

	double x_initial[2] = {d/2, M_PI/2};
	gsl_vector* x = gsl_vector_alloc(dimension);

	gsl_vector_set(x, 0, x_initial[0]);
	gsl_vector_set(x, 1, x_initial[1]);

	typeSolveur = gsl_multiroot_fdfsolver_hybridsj;
	solveur = gsl_multiroot_fdfsolver_alloc(typeSolveur, 2);
	gsl_multiroot_fdfsolver_set(solveur, &f, x);

	do{
		iteration++;
		status = gsl_multiroot_fdfsolver_iterate(solveur);

		if(status){break;}

		status = gsl_multiroot_test_residual(solveur->f, 1e-64);
	} while(status == GSL_CONTINUE && iteration < 100 && fabs(gsl_vector_get(solveur->x, 0)*cos(gsl_vector_get(solveur->x, 1))) < 20*d && fabs(gsl_vector_get(solveur->x, 0)*sin(gsl_vector_get(solveur->x, 1))) < 20*d);

	if(status == 0 && fabs(gsl_vector_get(solveur->x, 0)*cos(gsl_vector_get(solveur->x, 1))) < 20*d && fabs(gsl_vector_get(solveur->x, 0)*sin(gsl_vector_get(solveur->x, 1))) < 20*d){
		afficher_statusAJ(solveur);
	} else{
		printf("Aucun équilibre électrostatique n'a été trouvé.\n");
	}

	gsl_multiroot_fdfsolver_free(solveur);
	gsl_vector_free(x);

	return 1;
}
