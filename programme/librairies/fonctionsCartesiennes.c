/* on inclut tout ce dont à a besoin */
#include "librairiesExternes.h"
#include "fonctionsCartesiennes.h"
#include "utilitaires.h"

/* fonctions pour le calcul de la distance, du potentiel et du champ */

double distanceVersionCooCartesiennes(COOCARTESIENNES point1, COOCARTESIENNES point2){
	return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
}

double potentielVersionCartesienne(COOCARTESIENNES point, LISTECHARGESENCARTESIEN charges){
	double V = 0; // en e/A pour l'instant
	for(int i=0 ; i<charges.nombre ; i++){ // on parcourt toutes les charges et on additionne car il y a principe de superposition
		V = V + charges.tableauDesCharges[i].valeur*pow(GSL_CONST_MKSA_ELECTRON_CHARGE, -1)/(distanceVersionCooCartesiennes(point, charges.tableauDesCharges[i].coo)*pow(10, 10));
	}
	return CoulombSympa*V; // on "ajoute" la constante de coulomb en VA/e et on obtient le potentiel en V
}

VECTEUR2DCARTESIEN champVersionCartesienne(COOCARTESIENNES point, LISTECHARGESENCARTESIEN charges){
	double Ex = 0; // en Ae/(A^3) = e/(A^2) pour l'instant
	for(int i=0 ; i<charges.nombre ; i++){ // même principe que pour le potentiel sauf que ici il y a 2 composantes
		Ex = Ex + ((point.x - charges.tableauDesCharges[i].coo.x)*pow(10, 10))*charges.tableauDesCharges[i].valeur*pow(GSL_CONST_MKSA_ELECTRON_CHARGE, -1)/pow(distanceVersionCooCartesiennes(point, charges.tableauDesCharges[i].coo)*pow(10, 10), 3);
	}

	double Ey = 0; // en e/(A^3) pour l'instant
	for(int i=0 ; i<charges.nombre ; i++){
		Ey = Ey + ((point.y - charges.tableauDesCharges[i].coo.y)*pow(10, 10))*charges.tableauDesCharges[i].valeur*pow(GSL_CONST_MKSA_ELECTRON_CHARGE, -1)/pow(distanceVersionCooCartesiennes(point, charges.tableauDesCharges[i].coo)*pow(10, 10), 3);
	}

	VECTEUR2DCARTESIEN vecteur;
	vecteur.composanteX = CoulombSympa*Ex*pow(10, 10); // on a le champ en x "CoulombSympa*Ex" en V/A donc on le transforme en V/m
	vecteur.composanteY = CoulombSympa*Ey*pow(10, 10);
	return vecteur;
}

/* fonctions pour le calcul de la taille de cadre dynamiquement à la liste des charges donnée */

// donne la coordonnée x de la charge la plus à "droite", auteur : Dorian BARRERE
double chargeLaPlusADroite(LISTECHARGESENCARTESIEN charges){
	double x_max = charges.tableauDesCharges[0].coo.x;
	for(int i=1 ; i<charges.nombre ; i++){
		if(x_max < charges.tableauDesCharges[i].coo.x){
			x_max = charges.tableauDesCharges[i].coo.x;
		}
	}
	return x_max;
}

// donne la coordonnée x de la charge la plus à "gauche", auteur : Dorian BARRERE
double chargeLaPlusAGauche(LISTECHARGESENCARTESIEN charges){
	double x_min = charges.tableauDesCharges[0].coo.x;
	for(int i=1 ; i<charges.nombre ; i++){
		if(x_min > charges.tableauDesCharges[i].coo.x){
			x_min = charges.tableauDesCharges[i].coo.x;
		}
	}
	return x_min;
}

// donne la coordonnée y de la charge la plus à "haute", auteur : Dorian BARRERE
double chargeLaPlusHaute(LISTECHARGESENCARTESIEN charges){
	double y_max = charges.tableauDesCharges[0].coo.y;
	for(int i=1 ; i<charges.nombre ; i++){
		if(y_max < charges.tableauDesCharges[i].coo.y){
			y_max = charges.tableauDesCharges[i].coo.y;
		}
	}
	return y_max;
}

// donne la coordonnée y de la charge la plus "en bas", auteur : Dorian BARRERE
double chargeLaPlusBasse(LISTECHARGESENCARTESIEN charges){
	double y_min = charges.tableauDesCharges[0].coo.y;
	for(int i=1 ; i<charges.nombre ; i++){
		if(y_min < charges.tableauDesCharges[i].coo.y){
			y_min = charges.tableauDesCharges[i].coo.y;
		}
	}
	return y_min;
}

float genererXMAX(LISTECHARGESENCARTESIEN charges){
	float x_max;
	if(charges.nombre == 1 && charges.tableauDesCharges[0].coo.x == 0 && charges.tableauDesCharges[0].coo.y == 0){
		x_max = 1;
	} else{
		x_max = 2.2*MAX(MAX(fabs(chargeLaPlusADroite(charges)), fabs(chargeLaPlusAGauche(charges))), MAX(fabs(chargeLaPlusHaute(charges)), fabs(chargeLaPlusBasse(charges))));
	}
	return x_max;
}

// retourne 1 si selon le rayon minimal le point donné est trop proche de une charge dans la liste donnée, auteur : Dorian BARRERE
int estTropProcheDeUneCharge(COOCARTESIENNES point, LISTECHARGESENCARTESIEN charges, double rayonMinimal){
	int resultat = 0;

	for(int i=0 ; i<charges.nombre ; i++){ // on parcourt toutes les charges
		if(distanceVersionCooCartesiennes(point, charges.tableauDesCharges[i].coo) < rayonMinimal){
			resultat = 1;
		}
	}

	return resultat;
}

/* fonctions de génération des fichiers */

int genererFichiersPotentielExact(LISTECHARGESENCARTESIEN charges){
	/* on définit la taille du cadre. Celui-ci est toujours centré en 0, donc la liste des charges doit l'être aussi pour obtenir quelque chose de convenable !!! */
	float x_max = genererXMAX(charges);
	float x_min = -x_max;
	float y_max = x_max;
	float y_min = -x_max;

	double potentielMax = 0; // pour l'instant à le met à 0, une fois définit il nous servira générer dynamiquement les EQ en fonction de la liste des charges

	float recurence = 50; // le nombre de point sur un axe où l'on va calculer le potentiel

	COOCARTESIENNES point;

	FILE* fichier = fopen("./fichiersGeneres/exact_potentiel.dat", "w"); // on ouvre/crée le fichier dat pour le potentiel
	if(fichier==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf(ROUGE "\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n" NORMAL);
		return 0;
	}

	fprintf(fichier, "# x (m) ; y (m) ; potentiel (V)\n"); // l'en tête du fichier

	for(float i=x_min ; i<=x_max ; i=i+(x_max+fabsf(x_min))/recurence){ // on parcourt recurence fois, à intervalle régulier, l'axe de x du cadre
		for(float j=y_min ; j<=y_max ; j=j+(y_max+fabsf(y_min))/recurence){ // même principe mais pour l'axe des y
			point.x = i;
			point.y = j;

			if(!estTropProcheDeUneCharge(point, charges, x_max/25)){ // si on est pas trop proche d'une charge on calcule le potentiel
				if(potentielMax < fabs(potentielVersionCartesienne(point, charges))){
					potentielMax = fabs(potentielVersionCartesienne(point, charges));
				}
				fprintf(fichier, "%e;%e;%e\n", i, j, potentielVersionCartesienne(point, charges));
			}
		}
	}

	fclose(fichier); // on ferme le fichier

	fichier = fopen("./programme/scripts/exact_potentiel.sh", "w"); // on ouvre/crée le fichier script gnuplot
	if(fichier==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf(ROUGE "\n-!- erreur : le fichier du script n'a pas pu être ouvert ni généré -!-\n" NORMAL);
		return 0;
	}

	/* on génère le script dynamiquement en utilisant en outre le potentiel max */

	fputs("#!/bin/bash\n\n", fichier);
	fputs("gnuplot << EOF\n\n\t", fichier);
	fputs("set datafile separator \";\"\n\t", fichier);
	fprintf(fichier, "set xr [%e:%e]\n\t", -x_max, x_max);
	fprintf(fichier, "set yr [%e:%e]\n\t", -x_max, x_max);
	fputs("set dgrid3d 50,50,3\n\n\t", fichier);

	fputs("unset surface\n\t", fichier);
	fputs("set contour base\n\t", fichier);
	fprintf(fichier, "set cntrparam levels incremental %e,%e,%e\n\n\t", -potentielMax*0.07, potentielMax*0.007, potentielMax*0.07);

	fputs("set table \"./fichiersGeneres/equipot.tmp\"\n\t", fichier);
	fputs("splot \"./fichiersGeneres/exact_potentiel.dat\" u 1:2:3 w l\n\t", fichier);
	fputs("unset table\n\n\t", fichier);

	fputs("unset contour\n\t", fichier);
	fputs("set surface\n\n\t", fichier);

	fputs("set table \"./fichiersGeneres/d3grid.tmp\"\n\t", fichier);
	fputs("splot \"./fichiersGeneres/exact_potentiel.dat\" u 1:2:3\n\t", fichier);
	fputs("unset table\n\n\t", fichier);

	fputs("reset\n\n\t", fichier);

	for(int i=0 ; i<charges.nombre ; i++){ // on génère tous les labels
		fprintf(fichier, "set label \"q%d\" at %e,%e,0 front point pointsize 5 lw 5\n\t", i+1, charges.tableauDesCharges[i].coo.x, charges.tableauDesCharges[i].coo.y);
	}

	fputs("unset key\n\t", fichier);
	fputs("set size ratio 1\n\t", fichier);
	fputs("set size 0.95, 0.95\n\t", fichier);
	fputs("set origin 0.025, 0.025\n\t", fichier);
	fputs("set title \"Carte de Chaleur du Potentiel (en V) Exact\" font \",60\"\n\t", fichier);
	fputs("set terminal png size 3840,3840 font \",40\"\n\t", fichier);
	fputs("set output \"./fichiersGeneres/exact_potentiel.png\"\n\t", fichier);
	fputs("set xlabel \"x (en m)\" font \",50\"\n\t", fichier);
	fputs("set ylabel \"y (en m)\" font \",50\"\n\t", fichier);
	fputs("set border linewidth 8\n\n\t", fichier);

	fputs("\n\tset autoscale xfix\n\t", fichier);
	fputs("set autoscale yfix\n\n\t", fichier);

	fputs("set palette rgb 21,22,23\n\t", fichier);
	fputs("set pm3d map\n\t", fichier);
	fputs("set pm3d interpolate 0,0\n\n\t", fichier);

	//fputs("splot \"./fichiersGeneres/d3grid.tmp\" with pm3d, \"./fichiersGeneres/equipot.tmp\" w l lw 5\n\n", fichier); // le problème avec cette ligne c'est que j'ai essayé de gérer moi même les paramètres pour générer les équipotentielles, de façon à ce que cela me convienne pour tous les cas possibles que peut entrer l'utilisateur. Seuleument, ça marche pas hyper bien, et il y a des cas où gnuplot trace des lignes en trop transversales...
	fputs("splot \"./fichiersGeneres/d3grid.tmp\" with pm3d\n\n", fichier);

	fputs("EOF\n\n", fichier);
	fputs("rm \"./fichiersGeneres/equipot.tmp\"\n", fichier);
	fputs("rm \"./fichiersGeneres/d3grid.tmp\"\n\n", fichier);
	fputs("exit $?\n\n", fichier);

	fclose(fichier);

	system("./programme/scripts/exact_potentiel.sh"); // on lance le script

	return 1; // si on est ici tout s'est bien passé
}

int genererFichiersChampExact(LISTECHARGESENCARTESIEN charges){

	/* fonction trés similaire à genererFichiersPotentielExact() mais pour le champ */

	float x_max = genererXMAX(charges);
	float x_min = -x_max;
	float y_max = x_max;
	float y_min = -x_max;
	double longueurMax; // ici on prend pas le potentiel max mais la longueur maximal du vecteur que l'on est supposé tracé (en m)

	float recurence = 50;

	COOCARTESIENNES point;

	FILE* fichier = fopen("./fichiersGeneres/exact_champ.dat", "w");
	if(fichier==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf(ROUGE "\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n" NORMAL);
		return 0;
	}

	fprintf(fichier, "# x (m) ; y (m) ; composanteX du vecteur (N/C) ; composanteY du vecteur (N)\n");

	for(float i=x_min ; i<=x_max ; i=i+(x_max+fabsf(x_min))/recurence){
		for(float j=y_min ; j<=y_max ; j=j+(y_max+fabsf(y_min))/recurence){
			point.x = i;
			point.y = j;

			if(!estTropProcheDeUneCharge(point, charges, x_max/5)){
				if(longueurMax < sqrt(pow(champVersionCartesienne(point, charges).composanteX, 2) + pow(champVersionCartesienne(point, charges).composanteY, 2))){
					longueurMax = sqrt(pow(champVersionCartesienne(point, charges).composanteX, 2) + pow(champVersionCartesienne(point, charges).composanteY, 2));
				}
				fprintf(fichier, "%e;%e;%e;%e\n", i, j, champVersionCartesienne(point, charges).composanteX, champVersionCartesienne(point, charges).composanteY);
			}
		}
	}

	fclose(fichier); // on ferme le fichier

	fichier = fopen("./programme/scripts/exact_champ.sh", "w");
	if(fichier==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf(ROUGE "\n-!- erreur : le fichier du script n'a pas pu être ouvert ni généré -!-\n" NORMAL);
		return 0;
	}

	fputs("#!/bin/bash\n\n", fichier);
	fputs("gnuplot << EOF\n\n\t", fichier);
	fputs("unset key\n\t", fichier);
	fputs("set datafile separator \";\"\n\t", fichier);
	fputs("set size ratio 1\n\t", fichier);
	fputs("set size 0.95, 0.95\n\t", fichier);
	fputs("set origin 0.025, 0.025\n\t", fichier);
	fputs("set title \"Champ Vectoriel Électrique (en N/C) Exact\" font \",60\"\n\t", fichier);
	fputs("set terminal png size 3840,3840 font \",40\"\n\t", fichier);
	fputs("set output \"./fichiersGeneres/exact_champ.png\"\n\t", fichier);
	fputs("set xlabel \"x (en m)\" font \",50\"\n\t", fichier);
	fputs("set ylabel \"y (en m)\" font \",50\"\n\t", fichier);
	fputs("set border linewidth 8\n\n\t", fichier);
	fputs("set autoscale xfix\n\t", fichier);
	fputs("set autoscale yfix\n\n\t", fichier);

	for(int i=0 ; i<charges.nombre ; i++){
		fprintf(fichier, "set label \"q%d\" at %e,%e,0 front point pointsize 5 lw 5\n\t", i+1, charges.tableauDesCharges[i].coo.x, charges.tableauDesCharges[i].coo.y);
	}

	/* pour placer un label où on veut pour faire des tests
	fprintf(fichier, "set label \"point\" at %e,%e,0 front point pointsize 5 lw 5\n\t", -0.575e-10, -0.575e-10);*/

	double coefMiseALEchelle = longueurMax/(0.1*x_max); // il permet comme son nom l'indique d'utiliser la longueur max du vecteur que l'on est sensé tracé, pour avoir un coef qui met à l'échelle lisible tous les vecteurs pour le graphique gnuplot

	fprintf(fichier, "\n\tplot \"./fichiersGeneres/exact_champ.dat\" using 1:2:(\\$3/%e):(\\$4/%e) with vectors lc \"red\" lw 3\n\n", coefMiseALEchelle, coefMiseALEchelle);
	fputs("EOF\n\n", fichier);
	fputs("exit $?\n\n", fichier);

	fclose(fichier);

	system("./programme/scripts/exact_champ.sh"); // on lance le script

	return 1;
}

/* fonctions nécessaires aux fonctions qui permettent de trouver un point d'équilibre. Elles sont toutes fortement inspirées de la documentation de la librairie GSL section "Multidimensional Root-Finding" */

// fonction qui représente la fonction du champ électrique, auteur : Dorian BARRERE
int champCartesien_f(const gsl_vector* x, void* parametres, gsl_vector* f){
	LISTECHARGESENCARTESIEN charges = *((LISTECHARGESENCARTESIEN*)parametres);

	COOCARTESIENNES point;
	point.x = gsl_vector_get(x, 0);
	point.y = gsl_vector_get(x, 1);

	const double y0 = champVersionCartesienne(point, charges).composanteX;
	const double y1 = champVersionCartesienne(point, charges).composanteY;

	gsl_vector_set(f, 0, y0);
	gsl_vector_set(f, 1, y1);

	return GSL_SUCCESS;
}

// fonction qui représente la jacobienne de la fonction du champ électrique, auteur : Dorian BARRERE
int champCartesien_df(const gsl_vector* x, void* parametres, gsl_matrix* J){
	LISTECHARGESENCARTESIEN charges = *((LISTECHARGESENCARTESIEN*)parametres);

	COOCARTESIENNES point;
	point.x = gsl_vector_get(x, 0);
	point.y = gsl_vector_get(x, 1);

	/* on croise les doigts pour que les 4 DPP soient correctes ! */

	double df00 = 0; // en Ae/(A^3) = e/(A^2) pour l'instant
	for(int i=0 ; i<charges.nombre ; i++){
		df00 = df00 + charges.tableauDesCharges[i].valeur*pow(GSL_CONST_MKSA_ELECTRON_CHARGE, -1)*(1/(double)3)*(pow(charges.tableauDesCharges[i].coo.x*pow(10, 10), 2) - 2*point.x*pow(10, 10)*charges.tableauDesCharges[i].coo.x*pow(10, 10) + 3*pow(charges.tableauDesCharges[i].coo.y*pow(10, 10), 2) - 6*point.y*pow(10, 10)*charges.tableauDesCharges[i].coo.y*pow(10, 10) + pow(point.x*pow(10, 10), 2) + 3*pow(point.y*pow(10, 10), 2))/powf(distanceVersionCooCartesiennes(point, charges.tableauDesCharges[i].coo)*pow(10, 10), 8/(float)3);
	}
	df00 = CoulombSympa*df00*pow(10, 10); // on complète la formule avec la constante de coulomb et on transforme le tout en V/m

	double df01 = 0;
	for(int i=0 ; i<charges.nombre ; i++){
		df01 = df01 + -2*charges.tableauDesCharges[i].valeur*pow(GSL_CONST_MKSA_ELECTRON_CHARGE, -1)*(1/(double)3)*(charges.tableauDesCharges[i].coo.x - point.x)*pow(10, 10)*(charges.tableauDesCharges[i].coo.y - point.y)*pow(10, 10)/powf(distanceVersionCooCartesiennes(point, charges.tableauDesCharges[i].coo)*pow(10, 10), 8/(float)3);
	}
	df01 = CoulombSympa*df01*pow(10, 10);

	double df10 = df01;

	double df11 = 0;
	for(int i=0 ; i<charges.nombre ; i++){
		df11 = df11 + charges.tableauDesCharges[i].valeur*pow(GSL_CONST_MKSA_ELECTRON_CHARGE, -1)*(1/(double)3)*(3*pow(charges.tableauDesCharges[i].coo.x*pow(10, 10), 2) - 6*point.x*pow(10, 10)*charges.tableauDesCharges[i].coo.x*pow(10, 10) + pow(charges.tableauDesCharges[i].coo.y*pow(10, 10), 2) - 2*point.y*pow(10, 10)*charges.tableauDesCharges[i].coo.y*pow(10, 10) + 3*pow(point.x*pow(10, 10), 2) + pow(point.y*pow(10, 10), 2))/powf(distanceVersionCooCartesiennes(point, charges.tableauDesCharges[i].coo)*pow(10, 10), 8/(float)3);
	}
	df11 = CoulombSympa*df11*pow(10, 10);

	/* on met à jour à jacobienne avec les valeurs calculées juste avant */
	gsl_matrix_set (J, 0, 0, df00);
	gsl_matrix_set (J, 0, 1, df01);
	gsl_matrix_set (J, 1, 0, df10);
	gsl_matrix_set (J, 1, 1, df11);

	return GSL_SUCCESS;
}

// fonction qui représente la jacobienne de la fonction du champ électrique AINSI QUE la fonction elle même, auteur : Dorian BARRERE
int champCartesien_fdf(const gsl_vector* x, void* parametres, gsl_vector* f, gsl_matrix* J){

	champCartesien_f(x, parametres, f);
	champCartesien_df(x, parametres, J);

	return GSL_SUCCESS;
}

/* fonctions qui permettent de trouver un point d'équilibre. Elles sont toutes fortement inspirées de la documentation de la librairie GSL section "Multidimensional Root-Finding" */

int dejaTrouve(double x, double y, COOCARTESIENNES listePointsEquilibre[], int nombrePointsEquilibre){
	int dejaTrouve = 0;
	for(int i=0 ; i<nombrePointsEquilibre ; i++){
		if( fabs(listePointsEquilibre[i].x - x) < 1e-11 && fabs(listePointsEquilibre[i].y - y) < 1e-11){
			dejaTrouve = 1;
		}
	}
	return dejaTrouve;
}

int pointEquilibreExactSJ(LISTECHARGESENCARTESIEN charges){
	float x_max = genererXMAX(charges); // on récupère la taille du cadre
	float recurence = 50; // ainsi que la récurrence des points qui permettent de représenter la surface du cadre

	/* on déclare le solveur est son type */
	const gsl_multiroot_fsolver_type* typeSolveur;
	gsl_multiroot_fsolver* solveur;

	int status; // pour suivre l'évolution de la résolution
	size_t iteration = 0;

	const size_t dimension = 2; // notre problème est en dimension 2
	gsl_multiroot_function f = {&champCartesien_f, dimension, &charges};

	typeSolveur = gsl_multiroot_fsolver_hybrids; // l'algorithme utilisé est le "hybrids" de GSL qui est une version modifée de la méthode hybride de Powell
	solveur = gsl_multiroot_fsolver_alloc(typeSolveur, 2);

	gsl_vector* x = gsl_vector_alloc(dimension);
	COOCARTESIENNES point;

	/* le but ici va être de parcourir tout le mesh dont chaque point va être utilisé comme point initial. Cela permet de passer outre la forte sensibilité de la méthode à la définition du point initial... mais c'est assre lourd... */

	COOCARTESIENNES listePointsEquilibre[99];
	int nombrePointsEquilibre = 0;
	for(float i=-x_max ; i<=x_max ; i=i+(2*x_max)/(recurence)){
		for(float j=-x_max ; j<=x_max ; j=j+(2*x_max)/(recurence)){
			point.x = i;
			point.y = j;

			if(!estTropProcheDeUneCharge(point, charges, x_max/25)){ // si on est pas trop proche d'une charge on admet que ce point initial est valide
				gsl_vector_set(x, 0, point.x);
				gsl_vector_set(x, 1, point.y);
				gsl_multiroot_fsolver_set(solveur, &f, x);

				do{ // on itére la méthode jusqu'à que l'on trouve une réponse (qu'elle soit correcte ou pas)
					iteration++;
					status = gsl_multiroot_fsolver_iterate(solveur);

					if(status){break;}

					status = gsl_multiroot_test_residual(solveur->f, 1e-3); // précision forte pour compenser le fait que l'utilisateur peut utiliser une combinaison d'odres de grandeurs (pour les charges et les distances) pas du tout raisonnables physiquement (et donc avoir pas exemple des valeurs moyennes de potentiels trés faibles). Néanmoins si on se place dans le cas physique avec comme ordre de grandeur l'Angstrom et la charge élémentaire, alors comme le champ devient élevé, une précision de 1e-1 est déjà physiquement très cool !
				} while(status == GSL_CONTINUE && iteration < 100 && fabs(gsl_vector_get(solveur->x, 0)) < x_max && fabs(gsl_vector_get(solveur->x, 1)) < x_max); // si on dépasse 100 itérations ou si on sort du cadre on arrête

				iteration = 0;

				if(status == 0 && (fabs(gsl_vector_get(solveur->x, 0)) < x_max && fabs(gsl_vector_get(solveur->x, 1)) < x_max && !dejaTrouve(gsl_vector_get(solveur->x, 0), gsl_vector_get(solveur->x, 1), listePointsEquilibre, nombrePointsEquilibre))){
					listePointsEquilibre[nombrePointsEquilibre].x = gsl_vector_get(solveur->x, 0);
					listePointsEquilibre[nombrePointsEquilibre].y = gsl_vector_get(solveur->x, 1);
					nombrePointsEquilibre++;
					afficher_statusSJ(solveur);
				}
			}

		}
	}

	if(nombrePointsEquilibre == 0){
		printf("   Aucun équilibre électrostatique n'a été trouvé.\n");
	}

	/* test avec le point initial personnalisé
	gsl_vector_set(x, 0, -1e-12);
	gsl_vector_set(x, 1, 0);
	gsl_multiroot_fsolver_set(solveur, &f, x);

	do{
		iteration++;
		status = gsl_multiroot_fsolver_iterate(solveur);

		if(status){break;}

		status = gsl_multiroot_test_residual(solveur->f, 1e-2);
		printf("%e, %e, %d\n", gsl_vector_get(solveur->x, 0), gsl_vector_get(solveur->x, 1), status);
	} while(status == GSL_CONTINUE && iteration < 100 && fabs(gsl_vector_get(solveur->x, 0)) < x_max && fabs(gsl_vector_get(solveur->x, 1)) < x_max);

	printf ("status = %s\n", gsl_strerror(status));*/

	/* calculer en 1 point pour faire des tests
	COOCARTESIENNES pointE;
	pointE.x = -1.909830056250520364287705e-11;
	pointE.y = 0;

	// IMPORTANT : mettre 6 décimales et 24 peut tout changer !
	// Par exemple : -l 1e-19 -0.5e-10 0 5e-19 0.5e-10 0 avec une précision de 1e-3 donne un point d'équilibre à (-1.909830e-11 ; ~0) -> Ex = -6394727e-04. Or si on le calcule on obtient -4.958879e+04.
	// La réponse : il suffit de forcer l'écriture de plus de décimale pour les récupérer et calcule le champ en (-1.909830056250520364287705e-11 ; 0). On obtient alors quelque de satisfaisant : Ex = -5.371570e-03 ! 

	printf("\n%e ; %e\n", champVersionCartesienne(pointE, charges).composanteX, champVersionCartesienne(pointE, charges).composanteY);*/

	/* on libère ce qu'on a alloué plus tôt */
	gsl_multiroot_fsolver_free(solveur);
	gsl_vector_free(x);

	return 1;
}

// tout à fait similaire dans la forme à la fonction précédente sauf qu'on fournit ici la jacobienne
int pointEquilibreExactAJ(LISTECHARGESENCARTESIEN charges){
	float x_max = genererXMAX(charges);
	float recurence = 50;

	const gsl_multiroot_fdfsolver_type* typeSolveur;
	gsl_multiroot_fdfsolver* solveur;

	int status;
	size_t iteration = 0;

	const size_t dimension = 2;
	gsl_multiroot_function_fdf f = {&champCartesien_f, &champCartesien_df, &champCartesien_fdf, dimension, &charges};

	typeSolveur = gsl_multiroot_fdfsolver_hybridsj;
	solveur = gsl_multiroot_fdfsolver_alloc(typeSolveur, 2);

	gsl_vector* x = gsl_vector_alloc(dimension);
	COOCARTESIENNES point;

	COOCARTESIENNES listePointsEquilibre[99];
	int nombrePointsEquilibre = 0;
	for(float i=-x_max ; i<=x_max ; i=i+(2*x_max)/recurence){
		for(float j=-x_max ; j<=x_max ; j=j+(2*x_max)/recurence){
			point.x = i;
			point.y = j;

			if(!estTropProcheDeUneCharge(point, charges, x_max/25)){
				gsl_vector_set(x, 0, point.x);
				gsl_vector_set(x, 1, point.y);
				gsl_multiroot_fdfsolver_set(solveur, &f, x);

				do{
					iteration++;
					status = gsl_multiroot_fdfsolver_iterate(solveur);

					if(status){break;}

					status = gsl_multiroot_test_residual(solveur->f, 1e-3);
				} while(status == GSL_CONTINUE && iteration < 100 && fabs(gsl_vector_get(solveur->x, 0)) < x_max && fabs(gsl_vector_get(solveur->x, 1)) < x_max);

				iteration = 0;

				if(status == 0 && (fabs(gsl_vector_get(solveur->x, 0)) < x_max && fabs(gsl_vector_get(solveur->x, 1)) < x_max) && !dejaTrouve(gsl_vector_get(solveur->x, 0), gsl_vector_get(solveur->x, 1), listePointsEquilibre, nombrePointsEquilibre)){
					listePointsEquilibre[nombrePointsEquilibre].x = gsl_vector_get(solveur->x, 0);
					listePointsEquilibre[nombrePointsEquilibre].y = gsl_vector_get(solveur->x, 0);
					nombrePointsEquilibre++;
					afficher_statusAJ(solveur);
				}
			}

		}
	}

	if(nombrePointsEquilibre == 0){
		printf("   Aucun équilibre électrostatique n'a été trouvé.\n");
	}

	/* test avec le point initial personnalisé
	gsl_vector_set(x, 0, -0.5e-10);
	gsl_vector_set(x, 1, -0.5e-10);
	gsl_multiroot_fdfsolver_set(solveur, &f, x);

	do{
		iteration++;
		status = gsl_multiroot_fdfsolver_iterate(solveur);

		if(status){break;}

		status = gsl_multiroot_test_residual(solveur->f, 1e-5);
		printf("%e, %e\n", gsl_vector_get(solveur->x, 0), gsl_vector_get(solveur->x, 1));
	} while(status == GSL_CONTINUE && iteration < 100 && fabs(gsl_vector_get(solveur->x, 0)) < x_max && fabs(gsl_vector_get(solveur->x, 1)) < x_max);


	printf ("status = %s\n", gsl_strerror(status));*/

	gsl_multiroot_fdfsolver_free(solveur);
	gsl_vector_free(x);

	return 1;
}
