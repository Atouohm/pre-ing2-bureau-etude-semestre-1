/* on inclut tout ce dont on a besoin */
#include "librairiesExternes.h"
#include "commandes.h"

void aideProgramme(){
	printf("\n");
	printf("--------------------------------------------\n");
	printf("|                   Aide                   |\n");
	printf("--------------------------------------------\n");
	printf("\n");
	printf("Les options génériques:\n");
	printf("	--aide : affiche l'aide détaillée\n");
	printf("	-e <1|2|3>: permet de chercher un point d'équilibre\n");
	printf("		- 1 : rechercher dans le cadre de l'approximation dipolaire\n");
	printf("		- 2 : rechercher dans le cadre exact\n");
	printf("		- 3 : lance tous les modes précédents\n");
	printf("\n");
	printf("Les options de génération :\n");
	printf("	-p <1|2|3|4> : utiliser la génération dans l'approximation dipolaire\n");
	printf("		- 1 : génère les fichiers du Champ Électrique\n");
	printf("		- 2 : génère les fichiers du Potentiel\n");
	printf("		- 3 : génère le graphique des LDC (Lignes De Champ) et des EQ (Équipotentielles)\n");
	printf("		- 4 : lance tous les modes précédents\n");
	printf("	-c <1|2|3> : utiliser la génération cartésienne\n");
	printf("		- 1 : génère les fichiers du Champ Électrique\n");
	printf("		- 2 : génère les fichiers du Potentiel\n");
	printf("		- 3 : lance tous les modes précédents\n");
	printf("\n");
	printf("Les options de paramétrage :\n");
	printf("	- OGLIGATOIRES POUR LE MODE -p ET LE MODE -e {1|3} :\n");
	printf("		- 1 : -d <valeur (Mètre)> : la distance entre les 2 charges\n");
	printf("		- 2 : -q <valeur (Coulomb)> : la valeur absolue des 2 charges\n");
	printf("	- OGLIGATOIRE POUR LE MODE -c ET LE MODE -e {2|3} :\n");
	printf("		- 1 : -l <valeur charge n°1 (Coulomb)> <coordonnée X charge n°1 (Mètre)> <coordonnée Y charge n°1 (Mètre)> ... : la liste des charges du système (le nombre de charges n'est pas limité)\n");
	printf("\n");
	printf(BLEUCLAIR "Commande pour tout générer : " NORMAL);
	printf(BLEUCLAIR SOULIGNE "./executable -p 4 -d 1e-10 -q 1e-19 -c 3 -l 1e-19 1e-10 0 -1e-19 -1e-10 0 -e 3" NORMAL);
	printf("\n");
}

/*
	Auteur : Dorian BARRERE
	Date : 04-12-2022
	Résumer : automatise la fin de la fonction recupererInstructions en libérant la mémoire si nécessaire
	Entrée : la valeur de retour souhaitée, le pointeur de pointeur vers la liste d'instructions, la chaine à libérer
	Sortie : la valeur de retour entrée en paramètre
*/
int finDeRecupererInstructions(int valeurRetour, LISTEINSTRUCTIONS** listeInstructions, char* chaineLecture){

	if(valeurRetour == 0){ // si on veut retourner une erreur on libère tout avant
		free((*listeInstructions)->lValeur->tableauDesCharges);
		free((*listeInstructions)->lValeur);
		free(*listeInstructions);
		*listeInstructions = NULL;
	}

	free(chaineLecture); // sinon on garde la liste d'instructions intacte et on libère juste la seconde variable (la chaine de lecture)
	return valeurRetour;
}

/*
	Auteur : Dorian BARRERE
	Date : 22-04-2023
	Résumer : initialise la liste d'instructions aux valeurs par défaut
	Entrée : le pointeur vers la liste d'instructions
	Sortie : rien
*/
void initialiserListeInstructions(LISTEINSTRUCTIONS* listeInstructions){
	listeInstructions->aideDrapeau = 0;
	listeInstructions->pDrapeau = 0;
	listeInstructions->pValeur = 0;
	listeInstructions->dDrapeau = 0;
	listeInstructions->dValeur = 0;
	listeInstructions->qDrapeau = 0;
	listeInstructions->qValeur = 0;
	listeInstructions->cDrapeau = 0;
	listeInstructions->cValeur = 0;
	listeInstructions->lDrapeau = 0;

	listeInstructions->lValeur = malloc(sizeof(LISTECHARGESENCARTESIEN));
	listeInstructions->lValeur->nombre = 0;
	listeInstructions->lValeur->tableauDesCharges = malloc(sizeof(CHARGEENCARTESIEN));

	listeInstructions->eDrapeau = 0;
	listeInstructions->eValeur = 0;
}

/*
	Auteur : Dorian BARRERE
	Date : 04-12-2022
	Résumer : réinitialise la liste d'instructions aux valeurs par défaut
	Entrée : le pointeur vers la liste d'instructions
	Sortie : rien
*/
void reInitialiserListeInstructions(LISTEINSTRUCTIONS* listeInstructions){
	listeInstructions->aideDrapeau = 0;
	listeInstructions->pDrapeau = 0;
	listeInstructions->pValeur = 0;
	listeInstructions->dDrapeau = 0;
	listeInstructions->dValeur = 0;
	listeInstructions->qDrapeau = 0;
	listeInstructions->qValeur = 0;
	listeInstructions->cDrapeau = 0;
	listeInstructions->cValeur = 0;
	listeInstructions->lDrapeau = 0;

	free(listeInstructions->lValeur->tableauDesCharges);
	free(listeInstructions->lValeur);
	listeInstructions->lValeur = malloc(sizeof(LISTECHARGESENCARTESIEN));
	listeInstructions->lValeur->nombre = 0;
	listeInstructions->lValeur->tableauDesCharges = malloc(sizeof(CHARGEENCARTESIEN));

	listeInstructions->eDrapeau = 0;
	listeInstructions->eValeur = 0;
}

int recupererInstructions(const int argc, const char* const argv[], LISTEINSTRUCTIONS** listeInstructions){
	/* avant tout, si il n'y a aucun argument/option, cela ne sert à rien de continuer */
	if(argc == 1){
		return 0;
	}

	/* on déclare et affecte les variables nécessaires pour la suite */
	(*listeInstructions) = malloc(sizeof(LISTEINSTRUCTIONS));
	initialiserListeInstructions(*listeInstructions);
	char* chaineLecture = malloc(128*sizeof(char)); // pour lire la commande
	char option; // pour savoir quelle option on a en face

	for(int indiceArgument=1 ; indiceArgument<argc ; indiceArgument++){ // on parcours toutes les cases de argv

		if(sscanf(argv[indiceArgument], "-%127s", chaineLecture) && strlen(chaineLecture)==1){ // si la case correspond à une option
			option = chaineLecture[0]; // on la récupère
		} else{ // sinon
			if(!strcmp(argv[indiceArgument], "--aide")){ // si c'est --aide c ok
				reInitialiserListeInstructions(*listeInstructions); // on annule tout
				(*listeInstructions)->aideDrapeau = 1; // on dit qu'on veut l'aide
				return finDeRecupererInstructions(1, listeInstructions, chaineLecture); // et on termine la fonction
			}
			return finDeRecupererInstructions(0, listeInstructions, chaineLecture); // mais si c'est pas --aide c'est la commande n'est pas valide
		}

		switch(option){ // suivant l'option récupérée on fait notre analyse
			case 'p': // -p alors 1 argument à la suite
				indiceArgument++; // on va le chercher
				if((*listeInstructions)->pDrapeau == 1 || indiceArgument >= argc || strlen(argv[indiceArgument])!=1 || argv[indiceArgument][0]<49 || argv[indiceArgument][0]>52){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // on vérifie que ya pas de soucis
				(*listeInstructions)->pValeur = atoi(argv[indiceArgument]); // on le récupère
				(*listeInstructions)->pDrapeau = 1; // on dit qu'on l'a récupéré
				break;
			case 'd':
				indiceArgument++;
				if((*listeInstructions)->dDrapeau == 1 || indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->dValeur, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->dDrapeau = 1;
				break;
			case 'q':
				indiceArgument++;
				if((*listeInstructions)->qDrapeau == 1 || indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->qValeur, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->qDrapeau = 1;
				break;
			case 'c':
				indiceArgument++; // on va le chercher
				if((*listeInstructions)->cDrapeau == 1 || indiceArgument >= argc || strlen(argv[indiceArgument])!=1 || argv[indiceArgument][0]<49 || argv[indiceArgument][0]>51){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->cValeur = atoi(argv[indiceArgument]);
				(*listeInstructions)->cDrapeau = 1;
				break;
			case 'l':
				/* on récupère les infos de la première charge (il doit y en avoir au moins une) */
				(*listeInstructions)->lValeur->nombre = 1;
				indiceArgument++;
				if((*listeInstructions)->lDrapeau == 1 || indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->lValeur->tableauDesCharges[0].valeur, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				indiceArgument++;
				if(indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->lValeur->tableauDesCharges[0].coo.x, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				indiceArgument++;
				if(indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->lValeur->tableauDesCharges[0].coo.y, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				
				/* on récupère les infos des toutes les charges additionnelles */
				indiceArgument++;
				int indiceCharge = 1;
				while(indiceArgument <= (argc-1) && sscanf(argv[indiceArgument], "-%1[a-zA-Z^\n]", chaineLecture)!=1){ // tant que on arrive pas à une nouvelle option ou à la fin de la commande
					(*listeInstructions)->lValeur->nombre++;
					(*listeInstructions)->lValeur->tableauDesCharges = realloc((*listeInstructions)->lValeur->tableauDesCharges, (*listeInstructions)->lValeur->nombre*sizeof(CHARGEENCARTESIEN));

					if((*listeInstructions)->lDrapeau == 1 || indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->lValeur->tableauDesCharges[indiceCharge].valeur, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
					indiceArgument++;
					if(indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->lValeur->tableauDesCharges[indiceCharge].coo.x, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
					indiceArgument++;
					if(indiceArgument >= argc || sscanf(argv[indiceArgument], "%le%1[^\n]", &(*listeInstructions)->lValeur->tableauDesCharges[indiceCharge].coo.y, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
					
					indiceArgument++;
					indiceCharge++;
				}
				indiceArgument--;

				(*listeInstructions)->lDrapeau = 1;
				break;
			case 'e':
				indiceArgument++; // on va le chercher
				if((*listeInstructions)->eDrapeau == 1 || indiceArgument >= argc || strlen(argv[indiceArgument])!=1 || argv[indiceArgument][0]<49 || argv[indiceArgument][0]>52){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->eValeur = atoi(argv[indiceArgument]);
				(*listeInstructions)->eDrapeau = 1;
				break;
			default: // l'option n'est pas reconnue donc erreur
				return finDeRecupererInstructions(0, listeInstructions, chaineLecture);
		}

	}

	if((*listeInstructions)->pDrapeau == 0 && (*listeInstructions)->cDrapeau == 0 && (*listeInstructions)->eDrapeau == 0){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // il faut au moins -p ou -c ou -e

	if((*listeInstructions)->pDrapeau == 1 && ((*listeInstructions)->dDrapeau == 0 || (*listeInstructions)->qDrapeau == 0)){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // si -p, on doit aussi avoir -d et -q

	if((*listeInstructions)->cDrapeau == 1 && (*listeInstructions)->lDrapeau == 0){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // si -c, on doit aussi avoir -l

	if((*listeInstructions)->eDrapeau == 1){
		if((*listeInstructions)->eValeur == 1 && ((*listeInstructions)->dDrapeau == 0 || (*listeInstructions)->qDrapeau == 0)){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // si -e 1, on doit aussi avoir -d et -q
		if((*listeInstructions)->eValeur == 2 && (*listeInstructions)->lDrapeau == 0){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // si -e 2, on doit aussi avoir -l
		if((*listeInstructions)->eValeur == 3 && ((*listeInstructions)->dDrapeau == 0 || (*listeInstructions)->qDrapeau == 0 || (*listeInstructions)->lDrapeau == 0)){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // si -e 3, on doit aussi avoir -d, -q et -l
	}

	return (finDeRecupererInstructions(1, listeInstructions, chaineLecture)); // on renvoie la liste d'instructions tout en vérifiant que tout est bon
}