#ifndef H_FONCTIONSCARTESIENNES
#define H_FONCTIONSCARTESIENNES

// structure représentant un point en coordonnées cartésiennes (en m)
typedef struct cooCartesiennes {
	double x;
	double y;
} COOCARTESIENNES;

// structure représentant une charge avec les coordonnées cartésiennes (en m et en C pour la valeur)
typedef struct chargeEnCartesien {
	COOCARTESIENNES coo;
	double valeur;
} CHARGEENCARTESIEN;

// structure représentant une liste de charges (en m et en C pour la valeur)
typedef struct listeChargesEnCartesien {
	int nombre;
	CHARGEENCARTESIEN* tableauDesCharges;
} LISTECHARGESENCARTESIEN;

// structure représentant un vecteur en coordonnées cartésiennes (en m)
typedef struct vecteur2dCartesien {
	double composanteX;
	double composanteY;
} VECTEUR2DCARTESIEN;

/*
	Auteur : Dorian BARRERE
	Date : 16-04-2023
	Résumer : fonction qui renvoie la valeur du potentiel en point en connaissant les charges qui rentrent en jeu
	Entrée : le point (en coordonnées cartésiennes) et la liste des charges (en coordonnées cartésiennes)
	Sortie : la valeur du potentiel au point donné
*/
double potentielVersionCartesienne(COOCARTESIENNES point, LISTECHARGESENCARTESIEN charges);

/*
	Auteur : Dorian BARRERE
	Date : 16-04-2023
	Résumer : fonction qui renvoie le vecteur du champ électrique en connaissant les charges qui rentrent en jeu
	Entrée : le point (en coordonnées cartésiennes) et la liste des charges (en coordonnées cartésiennes)
	Sortie : le vecteur du champ électrique au point donné
*/
VECTEUR2DCARTESIEN champVersionCartesienne(COOCARTESIENNES point, LISTECHARGESENCARTESIEN charges);

/*
	Auteur : Dorian BARRERE
	Date : 16-04-2023
	Résumer : fonction qui génére le fichier dat du potentiel ainsi que le graphique en carte de chaleur avec les équipotentielles
	Entrée : la liste des charges (en coordonnées cartésiennes)
	Sortie : 1 si ok, 0 sinon
*/
int genererFichiersPotentielExact(LISTECHARGESENCARTESIEN charges);

/*
	Auteur : Dorian BARRERE
	Date : 16-04-2023
	Résumer : fonction qui génére le fichier dat du champ ainsi que le graphique associé
	Entrée : la liste des charges (en coordonnées cartésiennes)
	Sortie : 1 si ok, 0 sinon
*/
int genererFichiersChampExact(LISTECHARGESENCARTESIEN charges);

/*
	Auteur : Dorian BARRERE
	Date : 17-04-2023
	Résumer : fonction qui cherche un point d'équilibre avec une liste de charges donnée. Elle affiche si elle en a trouvé ou non et les coordonnées si c'est le cas. Ici le calcul est fait en approximant la jacobienne.
	Entrée : la liste des charges (en coordonnées cartésiennes)
	Sortie : 1 si ok, 0 sinon
*/
int pointEquilibreExactSJ(LISTECHARGESENCARTESIEN charges);

/*
	Auteur : Dorian BARRERE
	Date : 17-04-2023
	Résumer : fonction qui cherche un point d'équilibre avec une liste de charges donnée. Elle affiche si elle en a trouvé ou non et les coordonnées si c'est le cas. Ici le calcul est fait en donnant la jacobienne exacte.
	Entrée : la liste des charges (en coordonnées cartésiennes)
	Sortie : 1 si ok, 0 sinon
*/
int pointEquilibreExactAJ(LISTECHARGESENCARTESIEN charges);

#endif