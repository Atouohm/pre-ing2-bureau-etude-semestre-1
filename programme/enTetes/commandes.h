#ifndef H_COMMANDES
#define H_COMMANDES

#include "fonctionsCartesiennes.h" // puisque on utilise LISTECHARGESENCARTESIEN

// la structure représentant la liste des instructions possiblent
typedef struct listeInstructions{
	int aideDrapeau; // les drapeaux signifient si l'instruction est à faire (1), ou non (0)
	int pDrapeau;
	int pValeur; // les valeurs sont les arguments des options et du drapeau associé
	int dDrapeau;
	double dValeur;
	int qDrapeau;
	double qValeur;
	int cDrapeau;
	int cValeur;
	int lDrapeau;
	int eDrapeau;
	int eValeur;
	LISTECHARGESENCARTESIEN* lValeur;
} LISTEINSTRUCTIONS;

// fonction qui affiche l'aide dans la console
void aideProgramme();

/*
	Auteur : Dorian BARRERE
	Date : 04-01-2023
	Résumer : fonction qui récupère les instructions et analyse la commande
	Entrée : le arc et le argv du main, un pointeur (de pointeur vers une liste d'instructions) NULL (il sera alloué et initialisé automatiquement dans la fonction)
	Sortie : 1 si la commande était correcte, 0 sinon
*/
int recupererInstructions(const int argc, const char* const argv[], LISTEINSTRUCTIONS** listeInstructions);

#endif