#ifndef H_FONCTIONSPOLAIRES
#define H_FONCTIONSPOLAIRES

// structure représentant un point en coordonnées polaires (en m et en radian pour l'angle)
typedef struct cooPolaires {
	double r;
	double theta; // par rapport à l'axe des x
} COOPOLAIRES;

// structure représentant une charge avec les coordonnées polaires (en m, radian et en C pour la valeur)
typedef struct chargeEnPolaire {
	COOPOLAIRES coo;
	double valeur;
} CHARGEENPOLAIRE;

// structure représentant une liste de charges (en m, radian et en C pour la valeur)
typedef struct listeChargesEnPolaire {
	int nombre;
	CHARGEENPOLAIRE* tableauDesCharges;
} LISTECHARGESENPOLAIRE;

// structure représentant un vecteur en coordonnées polaires (en m et radian)
typedef struct vecteur2dPolaire {
	double longueur;
	double angle; // par rapport à l'axe des x
} VECTEUR2DPOLAIRE;

/*
	Auteur : Dorian BARRERE
	Date : 14-04-2023
	Résumer : fonction qui renvoie la valeur du potentiel en point en connaissant les charges qui rentrent en jeu
	Entrée : le point (en coordonnées polaires) et la liste des charges (en coordonnées polaires)
	Sortie : la valeur du potentiel au point donné
*/
double potentielVersionCooPolaires(COOPOLAIRES point, LISTECHARGESENPOLAIRE charges);

/*
	Auteur : Dorian BARRERE
	Date : 14-04-2023
	Résumer : fonction qui renvoie le vecteur du champ électrique en connaissant les charges qui rentrent en jeu
	Entrée : le point (en coordonnées polaires) et la liste des charges (en coordonnées polaires)
	Sortie : le vecteur du champ électrique au point donné
*/
VECTEUR2DPOLAIRE champVersionCooPolaires(COOPOLAIRES point, LISTECHARGESENPOLAIRE charges);

/*
	Auteur : Dorian BARRERE
	Date : 15-04-2023
	Résumer : fonction qui génére le fichier dat du potentiel ainsi que le graphique en carte de chaleur avec les équipotentielles
	Entrée : la distance entre les 2 charges (en m) et leur charge (en C)
	Sortie : 1 si ok, 0 sinon
*/
int genererFichiersPotentielDansApproximationDipolaire(double d, double q);

/*
	Auteur : Dorian BARRERE
	Date : 15-04-2023
	Résumer : fonction qui génére le fichier dat du champ ainsi que le graphique associé
	Entrée : la distance entre les 2 charges (en m) et leur charge (en C)
	Sortie : 1 si ok, 0 sinon
*/
int genererFichiersChampDansApproximationDipolaire(double d, double q);

/*
	Auteur : Dorian BARRERE
	Date : 16-04-2023
	Résumer : fonction qui génére le graphique des LDC et des EQ avec un script gnuplot
	Entrée : la distance entre les 2 charges (en m)
	Sortie : 1 si ok, 0 sinon
*/
int genererGrapheLdcEtEq(double d);

/*
	Auteur : Dorian BARRERE
	Date : 16-04-2023
	Résumer : fonction qui cherche un point d'équilibre avec une liste de charges donnée. Elle affiche si elle en a trouvé ou non et les coordonnées si c'est le cas. Ici le calcul est fait en approximant la jacobienne.
	Entrée : la distance entre les 2 charges (en m) et leur charge (en C)
	Sortie : 1 si ok, 0 sinon
*/
int pointEquilibreDansApproximationDipolaireSJ(double d, double q);

/*
	Auteur : Dorian BARRERE
	Date : 16-04-2023
	Résumer : fonction qui cherche un point d'équilibre avec une liste de charges donnée. Elle affiche si elle en a trouvé ou non et les coordonnées si c'est le cas. Ici le calcul est fait en donnant la jacobienne exacte.
	Entrée : la distance entre les 2 charges (en m) et leur charge (en C)
	Sortie : 1 si ok, 0 sinon
*/
int pointEquilibreDansApproximationDipolaireAJ(double d, double q);

#endif