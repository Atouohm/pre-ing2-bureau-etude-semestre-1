#ifndef H_LIBRAIRIES_EXTERNES
#define H_LIBRAIRIES_EXTERNES

// les bibliothèques/librairies externes utilisées
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_multiroots.h>

#define BLEUCLAIR "\033[01;34m"
#define ROUGE "\033[00;31m"
#define SOULIGNE "\033[04m"
#define NORMAL "\033[00m"

#endif
