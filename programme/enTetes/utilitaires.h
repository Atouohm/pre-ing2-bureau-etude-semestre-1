#ifndef H_UTILITAIRES
#define H_UTILITAIRES

/* quelques macros utilent pour les fonctions présentent dans le programme */
#define COULOMB 1/(4*M_PI*GSL_CONST_MKSA_VACUUM_PERMITTIVITY)
#define CoefTransfoCoulombSympa GSL_CONST_MKSA_ELECTRON_CHARGE*pow(10, 10)
#define CoulombSympa CoefTransfoCoulombSympa*COULOMB
#define MAX(a,b) (a>b?a:b)

/*
	Auteur : Dorian BARRERE
	Date : 14-04-2023
	Résumer : fonction qui remet un angle à son équivalent dans l'intervalle ]-PI;PI]
	Entrée : l'angle en radian
	Sortie : l'angle équivalnt dans l'intervalle ]-PI;PI]
*/
double resterDansPI(double angle);

/*
	Auteur : Dorian BARRERE
	Date : 14-04-2023
	Résumer : fonction qui calcule l'angle des coordonnées polaire d'un point en cartésien (dans l'intervalle ]-PI;PI])
	Entrée : la composante en x puis la composante en y
	Sortie : l'angle représentatif dans l'intervalle ]-PI;PI]
*/
double angleCooPolaires(double x, double y);

/*
	Auteur : Dorian BARRERE
	Date : 17-04-2023
	Résumer : fonction qui affiche le status d'un solveur "multiroot finding" sans jacobienne de gsl
	Entrée : le solveur
	Sortie : rien
*/
void afficher_statusSJ(gsl_multiroot_fsolver* solveur);

/*
	Auteur : Dorian BARRERE
	Date : 17-04-2023
	Résumer : fonction qui affiche le status d'un solveur "multiroot finding" avec jacobienne de gsl
	Entrée : le solveur
	Sortie : rien
*/
void afficher_statusAJ(gsl_multiroot_fdfsolver* solveur);

#endif