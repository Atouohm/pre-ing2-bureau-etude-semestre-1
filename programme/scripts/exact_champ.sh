#!/bin/bash

gnuplot << EOF

	unset key
	set datafile separator ";"
	set size ratio 1
	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Champ Vectoriel Électrique (en N/C) Exact" font ",60"
	set terminal png size 3840,3840 font ",40"
	set output "./fichiersGeneres/exact_champ.png"
	set xlabel "x (en m)" font ",50"
	set ylabel "y (en m)" font ",50"
	set border linewidth 8

	set autoscale xfix
	set autoscale yfix

	set label "q1" at -5.000000e-11,0.000000e+00,0 front point pointsize 5 lw 5
	set label "q2" at 0.000000e+00,5.000000e-11,0 front point pointsize 5 lw 5
	set label "q3" at 5.000000e-11,0.000000e+00,0 front point pointsize 5 lw 5
	set label "q4" at 0.000000e+00,-5.000000e-11,0 front point pointsize 5 lw 5
	
	plot "./fichiersGeneres/exact_champ.dat" using 1:2:(\$3/2.004208e+23):(\$4/2.004208e+23) with vectors lc "red" lw 3

EOF

exit $?

