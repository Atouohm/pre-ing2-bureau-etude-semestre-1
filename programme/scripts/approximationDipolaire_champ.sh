#!/bin/bash

gnuplot << EOF

	unset key
	set datafile separator ";"
	set size ratio 1
	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Champ Vectoriel Électrique (en N/C) dans l'Approximation Dipolaire" font ",60"
	set terminal png size 3840,3840 font ",40"
	set output "./fichiersGeneres/approximationDipolaire_champ.png"
	set xlabel "x (en m)" font ",50"
	set ylabel "y (en m)" font ",50"
	set autoscale xfix
	set autoscale yfix
	set border linewidth 8

	set label "-q" at -$1/2,0 point pointsize 5 lw 5
	set label "+q" at $1/2,0 point pointsize 5 lw 5

	plot "./fichiersGeneres/approximationDipolaire_champ.dat" using 1:2:(\$5/$2):(\$6) with arrows linewidth 4 linecolor 'red'

EOF

exit $?
