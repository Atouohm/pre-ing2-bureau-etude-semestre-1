#!/bin/bash

gnuplot << EOF

	set datafile separator ";"
	set xr [-20*$1:20*$1]
	set yr [-20*$1:20*$1]
	set dgrid3d 50,50,3 # on définit un mesh pour gnuplot

	# on crée une table gnuplot qui va stocker les contours (les EQ ici donc)

	unset surface
	set contour base
	set cntrparam levels incremental -$2,$3,$2

	set table "./fichiersGeneres/equipot.tmp"
	splot "./fichiersGeneres/approximationDipolaire_potentiel.dat" u 1:2:5 w l
	unset table

	# on fait la même chose mais pour la surface elle même

	unset contour
	set surface

	set table "./fichiersGeneres/d3grid.tmp"
	splot "./fichiersGeneres/approximationDipolaire_potentiel.dat" u 1:2:5
	unset table

	# on reset et on va tracer 2 tables qu'on a généré

	reset

	unset key
	set size ratio 1
	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Carte de Chaleur du Potentiel (en V) dans l'Approximation Dipolaire" font ",60"
	set terminal png size 3840,3840 font ",40"
	set output "./fichiersGeneres/approximationDipolaire_potentiel.png"
	set xlabel "x (en m)" font ",50"
	set ylabel "y (en m)" font ",50"
	set border linewidth 8

	set label "-q" at -$1/2,0 center front point pointsize 5 lw 5
	set label "+q" at $1/2,0 center front point pointsize 5 lw 5
	set autoscale xfix
	set autoscale yfix

	set palette rgb 21,22,23
	set pm3d map
	set pm3d interpolate 0,0

	splot "./fichiersGeneres/d3grid.tmp" with pm3d

EOF

rm "./fichiersGeneres/equipot.tmp"
rm "./fichiersGeneres/d3grid.tmp"

exit $?
