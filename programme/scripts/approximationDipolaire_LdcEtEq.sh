#!/bin/bash

gnuplot << EOF

	set datafile separator ";"
	set size ratio 1
	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Lignes de Champ et Équipotentielles dans l'Approximation Dipolaire" font ",60"
	set terminal png size 3840,3840 font ",40"
	set output "./fichiersGeneres/approximationDipolaire_LdcEtEq.png"
	set xlabel "x (en m)" font ",50"
	set ylabel "y (en m)" font ",50"
	set xr [-20*$1:20*$1]
	set yr [-20*$1:20*$1]
	set border linewidth 8

	set label "-q" at -$1/2,0 front point pointsize 5 lw 5
	set label "+q" at $1/2,0 front point pointsize 5 lw 5

	set polar # on se met dans un repaire polaire
	unset raxis # mais on enlève l'axe de r
	unset rtics # ainsi que les tics de l'axe de r

	set key t r noautotitles spacing 1.5 # on met une légende

	plot $1*2*sin(t)*sin(t) w l lw 5 lc "aquamarine" title "Lignes de Champ", $1*2*sqrt(abs(cos(t))) w l lw 5 lc "forest-green" title "Équipotentielles", for [n=2:10] $1*2*n*sin(t)*sin(t) w l lw 5 lc "aquamarine", for [n=2:10] $1*2*n*sqrt(abs(cos(t))) w l lw 5 lc "forest-green", for [n=1:10] -$1*2*n*sqrt(abs(cos(t))) w l lw 5 lc "forest-green"

EOF

exit $?
