#!/bin/bash

gnuplot << EOF

	set datafile separator ";"
	set xr [-$2:$2]
	set yr [-$2:$2]
	set dgrid3d 50,50,3 # on définit un mesh pour gnuplot

	unset contour
	set surface

	set table "./fichiersGeneres/d3grid.tmp"
	splot "./fichiersGeneres/ecartRelatif.dat" u 1:2:5
	unset table

	# on reset et on prépare au tracé

	reset

	unset key
	set size ratio 1
	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Carte de Chaleur de l'Écart Relatif entre les valeurs de l'approximation et celles exactes" font ",60"
	set terminal png size 3840,3840 font ",40"
	set output "./fichiersGeneres/ecartRelatif.png"
	set xlabel "x (en m)" font ",50"
	set ylabel "y (en m)" font ",50"
	set border linewidth 8

	set label "-q" at -$1/2,0 center front point pointsize 5 lw 5 lc rgb "white" textcolor "white"
	set label "+q" at $1/2,0 center front point pointsize 5 lw 5 lc rgb "white" textcolor "white"
	set autoscale xfix
	set autoscale yfix

	set palette rgb 21,22,23
	set pm3d map
	set pm3d interpolate 0,0

	splot "./fichiersGeneres/d3grid.tmp" with pm3d

EOF

rm "./fichiersGeneres/d3grid.tmp"

exit $?
