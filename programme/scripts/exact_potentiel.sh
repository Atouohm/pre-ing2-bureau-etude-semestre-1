#!/bin/bash

gnuplot << EOF

	set datafile separator ";"
	set xr [-1.100000e-10:1.100000e-10]
	set yr [-1.100000e-10:1.100000e-10]
	set dgrid3d 50,50,3

	unset surface
	set contour base
	set cntrparam levels incremental -1.288094e+01,1.288094e+00,1.288094e+01

	set table "./fichiersGeneres/equipot.tmp"
	splot "./fichiersGeneres/exact_potentiel.dat" u 1:2:3 w l
	unset table

	unset contour
	set surface

	set table "./fichiersGeneres/d3grid.tmp"
	splot "./fichiersGeneres/exact_potentiel.dat" u 1:2:3
	unset table

	reset

	set label "q1" at -5.000000e-11,0.000000e+00,0 front point pointsize 5 lw 5
	set label "q2" at 0.000000e+00,5.000000e-11,0 front point pointsize 5 lw 5
	set label "q3" at 5.000000e-11,0.000000e+00,0 front point pointsize 5 lw 5
	set label "q4" at 0.000000e+00,-5.000000e-11,0 front point pointsize 5 lw 5
	unset key
	set size ratio 1
	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Carte de Chaleur du Potentiel (en V) Exact" font ",60"
	set terminal png size 3840,3840 font ",40"
	set output "./fichiersGeneres/exact_potentiel.png"
	set xlabel "x (en m)" font ",50"
	set ylabel "y (en m)" font ",50"
	set border linewidth 8

	
	set autoscale xfix
	set autoscale yfix

	set palette rgb 21,22,23
	set pm3d map
	set pm3d interpolate 0,0

	splot "./fichiersGeneres/d3grid.tmp" with pm3d

EOF

rm "./fichiersGeneres/equipot.tmp"
rm "./fichiersGeneres/d3grid.tmp"

exit $?

