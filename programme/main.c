/* inclusion des headers des bibliothèques externes */
#include "librairiesExternes.h"

/* inclusion de nos headers pour nos bibliothèques */
#include "commandes.h"
#include "fonctionsPolaires.h"
#include "fonctionsCartesiennes.h"
#include "utilitaires.h"
#include "ecartRelatif.h"

void finMain(int valeurRetour, LISTEINSTRUCTIONS** listeInstructions){
	/* on libère tout ce qui a été malloc */
	if((*listeInstructions)->lDrapeau == 1){
		free((*listeInstructions)->lValeur->tableauDesCharges);
		free((*listeInstructions)->lValeur);
	}
	free(*listeInstructions);
	*listeInstructions = NULL;

	exit(valeurRetour);
}

/* fonction main prenant en paramètre les options et arguments de la commande d'exécution. Tout est défini comme constant pour ne pas que cela soit modifié */
int main(const int argc, const char* const argv[]){

	// pour lancer le génération du graphique de l'écart relatif du potentiel : genererGraphiqueEcartRelatif();

	// commande pour tout générer : ./executable.exe -p 4 -d 1e-10 -q 1e-19 -c 3 -l 1e-19 1e-10 0 -1e-19 -1e-10 0 -e 3 (sur windows avec cygwin) OU ./executable -p 4 -d 1e-10 -q 1e-19 -c 3 -l 1e-19 1e-10 0 -1e-19 -1e-10 0 -e 3 (sur linux)

	LISTEINSTRUCTIONS* listeInstructions;
	if(recupererInstructions(argc, argv, &listeInstructions)){ // on récupère les instructions de la commande et on regarde si elle est valide (si oui on rentre dans ce if)

		if(listeInstructions->aideDrapeau){ // si il y avait --help on affiche l'aide et on quite
			aideProgramme();
		} else{ // sinon on analyse toutes les instructions

			if(listeInstructions->pDrapeau == 1){
				switch(listeInstructions->pValeur){
					case 1:
						printf("\nGénération du fichier de données \"approximationDipolaire_champ.dat\" ainsi que du graphique \"approximationDipolaire_champ.png\"...\n");
						if(!genererFichiersChampDansApproximationDipolaire(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
					case 2:
						printf("\nGénération du fichier de données \"approximationDipolaire_potentiel.dat\" ainsi que du graphique \"approximationDipolaire_potentiel.png\"...\n");
						if(!genererFichiersPotentielDansApproximationDipolaire(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
					case 3:
						printf("\nGénération du graphique \"approximationDipolaire_LdcEtEq.png\"...\n");
						if(!genererGrapheLdcEtEq(listeInstructions->dValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
					case 4:
						printf("\nGénération des fichiers suivants...\n");
						printf("-> \"approximationDipolaire_champ.dat\" ainsi que le graphique \"approximationDipolaire_champ.png\"\n");
						if(!genererFichiersChampDansApproximationDipolaire(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						printf("-> \"approximationDipolaire_potentiel.dat\" ainsi que le graphique \"approximationDipolaire_potentiel.png\"\n");
						if(!genererFichiersPotentielDansApproximationDipolaire(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						printf("-> le graphique \"approximationDipolaire_LdcEtEq.png\"\n");
						if(!genererGrapheLdcEtEq(listeInstructions->dValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
				}
			}

			if(listeInstructions->cDrapeau == 1){
				switch(listeInstructions->cValeur){
					case 1:
						printf("\nGénération du fichier de données \"exact_champ.dat\" ainsi que du graphique \"exact_champ.png\"...\n");
						if(!genererFichiersChampExact(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
					case 2:
						printf("\nGénération du fichier de données \"exact_potentiel.dat\" ainsi que du graphique \"exact_potentiel.png\"...\n");
						if(!genererFichiersPotentielExact(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
					case 3:
						printf("\nGénération des fichiers suivants...\n");
						printf("-> \"exact_champ.dat\" ainsi que le graphique \"exact_champ.png\"\n");
						if(!genererFichiersChampExact(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						printf("-> \"exact_potentiel.dat\" ainsi que le graphique \"exact_potentiel.png\"\n");
						if(!genererFichiersPotentielExact(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
				}
			}

			if(listeInstructions->eDrapeau == 1){
				switch(listeInstructions->eValeur){
					case 1:
						printf("\nRecherche d'un point d'équilibre dans l'approximation dipolaire...\n");
						printf("-> SANS fournir la Jacobienne :\n");
						if(!pointEquilibreDansApproximationDipolaireSJ(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						printf("-> AVEC la Jacobienne :\n");
						if(!pointEquilibreDansApproximationDipolaireAJ(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
					case 2:
						printf("\nRecherche d'un point d'équilibre dans le contexte cartésien exact...\n");
						printf("-> SANS fournir la Jacobienne :\n");
						if(!pointEquilibreExactSJ(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						printf("-> AVEC la Jacobienne :\n");
						if(!pointEquilibreExactAJ(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
					case 3:
						printf("\nRecherches de point d'équilibre...\n");

						printf("-> dans l'approximation dipolaire...\n");
						printf("   - SANS fournir la Jacobienne :\n");
						if(!pointEquilibreDansApproximationDipolaireSJ(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						printf("   - AVEC la Jacobienne :\n ");
						if(!pointEquilibreDansApproximationDipolaireAJ(listeInstructions->dValeur, listeInstructions->qValeur)){finMain(EXIT_FAILURE, &listeInstructions);}

						printf("-> dans le contexte cartésien exact...\n");
						printf("   - SANS fournir la Jacobienne :\n");
						if(!pointEquilibreExactSJ(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						printf("   - AVEC la Jacobienne :\n");
						if(!pointEquilibreExactAJ(*listeInstructions->lValeur)){finMain(EXIT_FAILURE, &listeInstructions);}
						break;
				}
			}

		}

		finMain(EXIT_SUCCESS, &listeInstructions);
	} else{ // si elle ne l'était pas alors erreur
		printf(ROUGE "\n-!- erreur dans la commande -!-\n" NORMAL);
		aideProgramme();
		exit(EXIT_FAILURE);
	}
}

int mainDeTest(const int argc, const char* const argv[]){
	double d = GSL_CONST_MKSA_ANGSTROM; // GSL_CONST_MKSA_ANGSTROM
	double q = GSL_CONST_MKSA_ELECTRON_CHARGE; // GSL_CONST_MKSA_ELECTRON_CHARGE

	LISTECHARGESENCARTESIEN charges;
	charges.nombre = 2;
	charges.tableauDesCharges = (CHARGEENCARTESIEN*)malloc(charges.nombre*sizeof(CHARGEENCARTESIEN));
	charges.tableauDesCharges[0].coo.x = d/2;
	charges.tableauDesCharges[0].coo.y = 0;
	charges.tableauDesCharges[0].valeur = -q;
	charges.tableauDesCharges[1].coo.x = -d;
	charges.tableauDesCharges[1].coo.y = 0;
	charges.tableauDesCharges[1].valeur = -q;
	charges.tableauDesCharges[2].coo.x = d;
	charges.tableauDesCharges[2].coo.y = 10*d;
	charges.tableauDesCharges[2].valeur = q;
	charges.tableauDesCharges[3].coo.x = -d;
	charges.tableauDesCharges[3].coo.y = 10*d;
	charges.tableauDesCharges[3].valeur = q;

	genererFichiersPotentielExact(charges);

	genererFichiersChampExact(charges);

	pointEquilibreExactSJ(charges);

	pointEquilibreExactAJ(charges);

	genererFichiersPotentielDansApproximationDipolaire(d, q);

	genererFichiersChampDansApproximationDipolaire(d, q);

	pointEquilibreDansApproximationDipolaireSJ(d, q);

	pointEquilibreDansApproximationDipolaireAJ(d, q);

	return 0;
}
