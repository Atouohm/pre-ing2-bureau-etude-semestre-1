exe: ./programme/fichiersObjets/main.o ./programme/fichiersObjets/fonctionsPolaires.o ./programme/fichiersObjets/fonctionsCartesiennes.o ./programme/fichiersObjets/utilitaires.o ./programme/fichiersObjets/commandes.o ./programme/fichiersObjets/ecartRelatif.o
	@echo Création du fichier exécutable à partir des fichiers objets...
	@gcc -Wall ./programme/fichiersObjets/main.o ./programme/fichiersObjets/fonctionsPolaires.o ./programme/fichiersObjets/fonctionsCartesiennes.o ./programme/fichiersObjets/utilitaires.o ./programme/fichiersObjets/commandes.o ./programme/fichiersObjets/ecartRelatif.o -o executable -lm -lgsl -lgslcblas

./programme/fichiersObjets/main.o: ./programme/main.c
	@echo Compilation partielle de main.c en main.o...
	@gcc -Wall -I ./programme/enTetes/ -c ./programme/main.c -o ./programme/fichiersObjets/main.o

./programme/fichiersObjets/fonctionsPolaires.o: ./programme/librairies/fonctionsPolaires.c
	@echo Compilation partielle de fonctionsPolaires.c en fonctionsPolaires.o...
	@gcc -Wall -I ./programme/enTetes/ -c ./programme/librairies/fonctionsPolaires.c -o ./programme/fichiersObjets/fonctionsPolaires.o

./programme/fichiersObjets/fonctionsCartesiennes.o: ./programme/librairies/fonctionsCartesiennes.c
	@echo Compilation partielle de fonctionsCartesiennes.c en fonctionsCartesiennes.o...
	@gcc -Wall -I ./programme/enTetes/ -c ./programme/librairies/fonctionsCartesiennes.c -o ./programme/fichiersObjets/fonctionsCartesiennes.o

./programme/fichiersObjets/utilitaires.o: ./programme/librairies/utilitaires.c
	@echo Compilation partielle de utilitaires.c en utilitaires.o...
	@gcc -Wall -I ./programme/enTetes/ -c ./programme/librairies/utilitaires.c -o ./programme/fichiersObjets/utilitaires.o

./programme/fichiersObjets/commandes.o: ./programme/librairies/commandes.c
	@echo Compilation partielle de commandes.c en commandes.o...
	@gcc -Wall -I ./programme/enTetes/ -c ./programme/librairies/commandes.c -o ./programme/fichiersObjets/commandes.o

./programme/fichiersObjets/ecartRelatif.o: ./programme/librairies/ecartRelatif.c
	@echo Compilation partielle de ecartRelatif.c en ecartRelatif.o...
	@gcc -Wall -I ./programme/enTetes/ -c ./programme/librairies/ecartRelatif.c -o ./programme/fichiersObjets/ecartRelatif.o

nettoyer:
	@echo Suppression des .o temporaires créés pour la génération du exe...
	@rm -i ./programme/fichiersObjets/*.o
